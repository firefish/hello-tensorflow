from __future__ import division
from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals
import pandas as pd
import random
import json

TRAIN_FILE = 'base/data/train.txt'
DEV_FILE = 'base/data/dev.txt'
TEST_FILE = 'base/data/test.txt'
TRAIN_PART = pd.Interval(0.0, 0.1, closed=b'left')
DEV_PART = pd.Interval(0.1, 0.2, closed=b'left')
TEST_PART = pd.Interval(0.2, 1.0, closed=b'left')
NUM_TOKS = 'num_toks'
TXT_TOKS = 'txt_toks'
TOKEN_PAD = '<PAD>'
META_PAD = '<META>'
QUERY = '<QUERY>'

tokens_dict = {
    '0': 0,
    '1': 1,
    '2': 2,
    '3': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
    '9': 9,

    'a': 10,
    'b': 11,
    'c': 12,
    'd': 13,
    'e': 14,
    'f': 15,
    'g': 16,
    'h': 17,
    'i': 18,
    'j': 19,

    'inp_0': 20,
    'inp_1': 21,
    'inp_2': 22,
    'inp_3': 23,
    'inp_4': 24,
    'inp_5': 25,
    'inp_6': 26,
    'inp_7': 27,
    'inp_8': 28,
    'inp_9': 29,

    'outp_0': 30,
    'outp_1': 31,
    'outp_2': 32,
    'outp_3': 33,
    'outp_4': 34,
    'outp_5': 35,
    'outp_6': 36,
    'outp_7': 37,
    'outp_8': 38,
    'outp_9': 39,

    TOKEN_PAD: 40,
    META_PAD: 41,
    QUERY: 42,
}
tokens_rdict = {v: k for k, v in tokens_dict.items()}


def encode(s, pos, dir):
    return [tokens_dict[s], tokens_dict[dir + str(pos)]]


def pad(a, width=6):
    while len(a) < width:
        a.insert(0, TOKEN_PAD)
    return a


def main():
    with open(TRAIN_FILE, 'w') as train_f, open(DEV_FILE, 'w') as dev_f, open(TEST_FILE, 'w') as test_f:
        for sample in range(1000000):
            r = random.random()
            num_toks_arr = pad([c for c in str(sample)])
            txt_toks_arr = pad([chr(ord(c) - ord('0') + ord('a')) for c in str(sample)])
            num_toks_enc = [encode(x, i, 'inp_') for i, x in enumerate(num_toks_arr)]
            txt_toks_enc = [encode(x, i, 'outp_') for i, x in enumerate(txt_toks_arr)]
            recs = []
            for i in range(len(txt_toks_enc)):
                q = list(txt_toks_enc[i])
                y, q[0] = q[0], tokens_dict[QUERY]
                recs.append({'x': num_toks_enc + [q], 'y': y})
            for rec in recs:
                if r in TRAIN_PART:
                    print(json.dumps(rec), file=train_f)
                elif r in DEV_PART:
                    print(json.dumps(rec), file=dev_f)
                elif r in TEST_PART:
                    print(json.dumps(rec), file=test_f)
                else:
                    assert False
        pass
    pass


main()
