from __future__ import print_function
from num2words import num2words
import pandas as pd
import random

TRAIN_PART = pd.Interval(0.0, 0.1, closed=b'left')
DEV_PART = pd.Interval(0.1, 0.2, closed=b'left')
TEST_PART = pd.Interval(0.2, 1.0, closed=b'left')
with open('base/data/train.txt', 'w') as train_f, open('base/data/dev.txt', 'w') as dev_f, \
        open('base/data/test.txt', 'w') as test_f:
    for i in range(1000000):
        rnd = random.random()
        n = [x for x in str(i)]
        t = num2words(i).replace('-', ' ').replace(',', '').replace(' and ', ' ').split(' ')
        # s = ' '.join(reversed(n)) + '\t' + ' '.join(reversed(t))
        s = ' '.join(n) + '\t' + ' '.join(t)
        if rnd in TRAIN_PART:
            print(s, file=train_f)
        elif rnd in DEV_PART:
            print(s, file=dev_f)
        elif rnd in TEST_PART:
            print(s, file=test_f)
        else:
            assert False
