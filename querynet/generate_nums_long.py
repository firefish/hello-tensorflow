from __future__ import print_function
from num2words import num2words
import pandas as pd
import random

TRAIN_PART = pd.Interval(0.0, 0.1, closed=b'left')
DEV_PART = pd.Interval(0.1, 0.2, closed=b'left')
TEST_PART = pd.Interval(0.2, 1.0, closed=b'left')
random.seed(42)

with open('base/data/train.txt', 'w') as train_f, open('base/data/dev.txt', 'w') as dev_f, \
        open('base/data/test.txt', 'w') as test_f, open('base/data/full.txt', 'w') as full_f:
    u = set()
    for pos in range(15):
        for d in range(10):
            for i in range(100):
                n = random.randint((10 ** pos) * d, (10 ** pos) * (d + 1) - 1)
                u.add(n)
    for i in u:
        rnd = random.random()
        n = [x for x in str(i)]
        t = num2words(i).replace('-', ' ').replace(',', '').replace(' and ', ' ').split(' ')
        s = ' '.join(n) + '\t' + ' '.join(t)
        print(s, file=full_f)
        if rnd in TRAIN_PART:
            print(s, file=train_f)
        elif rnd in DEV_PART:
            print(s, file=dev_f)
        elif rnd in TEST_PART:
            print(s, file=test_f)
        else:
            assert False
