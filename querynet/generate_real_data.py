from __future__ import division
from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals
import pandas as pd
import random
import json

TRAIN_FILE = 'base/data/train.txt'
DEV_FILE = 'base/data/dev.txt'
TEST_FILE = 'base/data/test.txt'
DICT_FILE = 'base/data/dict.txt'
TRAIN_PART = pd.Interval(0.0, 0.1, closed=b'left')
DEV_PART = pd.Interval(0.1, 0.2, closed=b'left')
TEST_PART = pd.Interval(0.2, 1.0, closed=b'left')
NUM_TOKS = 'num_toks'
TXT_TOKS = 'txt_toks'
TOKEN_PAD = '<PAD>'
META_PAD = '<META>'
QUERY = '<QUERY>'

tokens_dict = {
    'inp_0': 0,
    'inp_1': 1,
    'inp_2': 2,
    'inp_3': 3,
    'inp_4': 4,
    'inp_5': 5,
    'inp_6': 6,
    'inp_7': 7,
    'inp_8': 8,
    'inp_9': 9,

    'outp_0': 10,
    'outp_1': 11,
    'outp_2': 12,
    'outp_3': 13,
    'outp_4': 14,
    'outp_5': 15,
    'outp_6': 16,
    'outp_7': 17,
    'outp_8': 18,
    'outp_9': 19,

    '0': 20,
    '1': 21,
    '2': 22,
    '3': 23,
    '4': 24,
    '5': 25,
    '6': 26,
    '7': 27,
    '8': 28,
    '9': 29,
    TOKEN_PAD: 90,
    META_PAD: 91,
    QUERY: 92,
}
cur_tok = 30
assert cur_tok not in tokens_dict


# tokens_rdict = {v: k for k, v in tokens_dict.items()}


def encode(s, pos, dir):
    return [tokens_dict[s], tokens_dict[dir + str(pos)]]


def pad(a, width=6):
    while len(a) < width:
        a.insert(0, TOKEN_PAD)
    return a


def upd_dict(toks):
    global cur_tok
    for tok in toks:
        if tok not in tokens_dict:
            tokens_dict[tok] = cur_tok
            cur_tok += 1


def main():
    with open('base/data/input.txt', 'r') as inp_f, open(TRAIN_FILE, 'w') as train_f, \
            open(DEV_FILE, 'w') as dev_f, open(TEST_FILE, 'w') as test_f, open(DICT_FILE, 'w') as dict_f:
        for ln in inp_f:
            l, r = ln.decode('utf8').rstrip().split('\t')

            num_toks_arr = pad([c for c in l])
            txt_toks_arr = pad([c for c in r.split(' ')], width=7)
            upd_dict(txt_toks_arr)

            num_toks_enc = [encode(x, i, 'inp_') for i, x in enumerate(num_toks_arr)]
            txt_toks_enc = [encode(x, i, 'outp_') for i, x in enumerate(txt_toks_arr)]

            recs = []
            for i in range(len(txt_toks_enc)):
                q = list(txt_toks_enc[i])
                y, q[0] = q[0], tokens_dict[QUERY]
                tail = [encode(META_PAD, j, 'outp_') for j in range(i)] + [q] + txt_toks_enc[i+1:]
                recs.append({'x': num_toks_enc + tail, 'y': y})

            rnd = random.random()
            for rec in recs:
                if rnd in TRAIN_PART:
                    print(json.dumps(rec), file=train_f)
                elif rnd in DEV_PART:
                    print(json.dumps(rec), file=dev_f)
                elif rnd in TEST_PART:
                    print(json.dumps(rec), file=test_f)
                else:
                    assert False
        pass
        print(json.dumps(tokens_dict, ensure_ascii=False).encode('utf8'), file=dict_f)
    pass


main()
