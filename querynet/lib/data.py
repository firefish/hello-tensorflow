from __future__ import division
from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals
from .token_property import property_funcs

PAD = '<PAD>'
MASKED = '<MASKED>'
DUMMY = '<DUMMY>'
QUERY = '<QUERY>'
POS_IN_FWD_PREFIX = '#pos_in_fwd_'
POS_IN_BWD_PREFIX = '#pos_in_bwd_'
POS_OUT_FWD_PREFIX = '#pos_out_fwd_'


class Base(object):
    """
    Class for common data in specific format properties.
    - length
    - dictionary
    - max_record_length
    """

    def __init__(self):
        self.data = []
        self.dict = {}

    def length(self):
        return len(self.data)

    def max_record_length(self):
        return max([len(x) for x in self.data])


class Raw(Base):
    """
    Record example:
    "txt_in", "txt_out"
    """

    @staticmethod
    def from_tsv(file_path, empty_dst=False):
        res = Raw()
        with open(file_path, 'r') as file_f:
            for line in file_f:
                first, second = line.rstrip().split('\t')
                first_arr = first.split(' ')
                second_arr = [] if empty_dst else second.split(' ')
                res.data.append([first_arr, second_arr])
        return res

    def max_record_length(self):
        return max([len(x[0]) + len(x[1]) for x in self.data])

    @staticmethod
    def dict_from_tsv_files(file_paths):
        res = Raw()
        len_in = 0
        len_out = 0
        d = {}
        uniq_id = 0
        for file_path in file_paths:
            with open(file_path, 'r') as file_f:
                for line in file_f:
                    first, second = line.decode('utf-8').rstrip().split('\t')
                    first_arr = first.split(' ')
                    second_arr = second.split(' ')
                    len_in = max(len_in, len(first_arr))
                    len_out = max(len_out, len(second_arr))
                    res.data.append([first_arr, second_arr])
                    for arr in [first_arr, second_arr]:
                        for tok in arr:
                            if tok not in d:
                                d[tok] = uniq_id
                                uniq_id += 1
        return d, len_in, len_out


# ToDo: multiple token properties per Record item
class Encoded(Base):
    """
    Record example:
    [[tok,pos_in],
     ...
     [tok,pos_in],
     [tok,pos_out],
     ...
     [tok,pos_out]]
    One-to-one relation with Raw format records
    """

    def _encode(self, tok_word, tok_pos_fwd, tok_pos_bwd):
        return [self.dict[tok_word], self.dict[tok_pos_fwd], self.dict[tok_pos_bwd]]

    @staticmethod
    def from_raw(data_raw, dict=None, pad_in=None, pad_out=None):
        """
        :type data_raw:Raw
        :type pad_in:int
        :type pad_out:int
        """
        res = Encoded()
        assert dict
        res.dict = dict

        for raw_rec in data_raw.data:
            # pad
            raw_in = (raw_rec[0] + [PAD for _ in range(pad_in)])[:pad_in]
            raw_out = (raw_rec[1] + [PAD for _ in range(pad_out)])[:pad_out]

            props_in = [func(raw_in, out=False) for func in property_funcs]
            props_out = [func(raw_out, out=True) for func in property_funcs]

            rec_in = [res._encode(*it) for it in zip(*props_in)]  # unpacks T_T
            rec_out = [res._encode(*it) for it in zip(*props_out)]  # unpacks T_T
            res.data.append(rec_in + rec_out)
        return res

    @staticmethod
    def make_dict(pad_in, pad_out):
        d = {}

        uniq_id = 0
        d[PAD] = uniq_id
        uniq_id += 1
        for i in range(pad_in):
            d[POS_IN_FWD_PREFIX + str(i)] = uniq_id
            uniq_id += 1
            d[POS_IN_BWD_PREFIX + str(i)] = uniq_id
            uniq_id += 1

        for i in range(pad_out):
            d[POS_OUT_FWD_PREFIX + str(i)] = uniq_id
            uniq_id += 1

        d[DUMMY] = uniq_id
        return d

    def to_human_readable(self):
        rev_dict = {v: k for k, v in self.dict.items()}
        return [[[rev_dict[tok] for tok in toks] for toks in rec] for rec in self.data]


class Dataset(Base):
    """
    Record example:
    {
    'x':[[tok,pos_in],
         ...
         [tok,pos_in],
         [tok,pos_out],
         ...
         [tok,pos_out],
         [query,pos_out],
         [masked,pos_out],
         ...
         [masked,pos_out]],
    'y':tok
    }
    Many-to-one relation with Encoded format.
    One record for each out token position
    """

    def _replace_tok(self, rec, tok):
        rec_copy = list(rec)
        rec_copy[0] = self.dict[tok]
        return rec_copy

    @staticmethod
    def _get_tok(rec):
        return rec[0]

    def _mask(self, recs):
        return [self._replace_tok(x, MASKED) for x in recs]

    @staticmethod
    def from_encoded(data_encoded, cnt=999999):
        """
        :type data_encoded:Encoded
        :type cnt:int
        """
        res = Dataset()
        res.dict = data_encoded.dict.copy()

        rev_dict = {v: k for k, v in res.dict.items()}
        for encoded_rec in data_encoded.data:
            rec = []
            for tok_pos in range(len(encoded_rec)):
                tok = encoded_rec[tok_pos]
                # or POS_OUT_BWD_PREFIX. Just skip in.
                if len(rec) < cnt and any(rev_dict[x].startswith(POS_OUT_FWD_PREFIX) for x in tok):
                    rec.append({
                        'x': encoded_rec[:tok_pos] + [res._replace_tok(tok, QUERY)] + res._mask(
                            encoded_rec[tok_pos + 1:]),
                        'y': res._get_tok(tok)
                    })
            res.data.append(rec)
        return res

    @staticmethod
    def make_dict():
        d = {}
        uniq_id = 0
        for tok in QUERY, MASKED:
            d[tok] = uniq_id
            uniq_id += 1
        return d

    def to_human_readable(self):
        rev_dict = {v: k for k, v in self.dict.items()}
        return [[
            {'x': [[rev_dict[tok] for tok in toks] for toks in pos['x']], 'y': rev_dict[pos['y']]}
            for pos in rec] for rec in self.data]
