import tensorflow as tf


class QueryNet(object):
    def build_model_fn(self):
        def model_fn(features, labels, mode):
            x = features['x']  # (?,seq_len,prop_cnt)
            weights = features['weights'] if 'weights' in features else 1.0
            m = self.dense_multiplier
            r = self.regularization
            query = tf.Variable(tf.random_uniform([self.width], -1, 1), name='emb_query')  # (width,)
            emb_tok = tf.Variable(tf.random_uniform([self.dict_size, self.width], -1, 1), name='emb_tok')  # (dict_size,width)

            tm_emb = tf.nn.embedding_lookup(emb_tok, x[:, :, 0])  # (?,seq_len,width)
            for i in range(1, x.get_shape()[-1]):
                tm_emb += tf.nn.embedding_lookup(emb_tok, x[:, :, i])

            query_dot = tf.reduce_mean(tm_emb * query, axis=2)  # (?,seq_len)
            query_attn = tf.nn.softmax(query_dot, name='query_attn')  # (?,seq_len)
            query_seen = tf.reduce_mean(
                tf.expand_dims(query_attn, axis=2) * tm_emb,  # (?,seq_len,width)
                axis=1,
                name='query_seen')  # (?,width)

            toks_seen = []
            for i in range(self.attn_count):
                attn_dense1 = tf.layers.dense(query_seen, m * self.width, activation=tf.nn.relu)  # (?,width*m)
                attn_dense2 = tf.layers.dense(attn_dense1, self.width, activation=tf.nn.relu)  # (?,width)

                attn_dot = tf.reduce_mean(tm_emb * tf.expand_dims(attn_dense2, axis=1), axis=2)  # (?,seq_len)
                attn = tf.nn.softmax(attn_dot, name='attn' + str(i))  # (?,seq_len)

                tok_seen = tf.reduce_mean(tm_emb * tf.expand_dims(attn, axis=2), axis=1, name='tok_seen' + str(i))  # (?,width)
                toks_seen.append(tok_seen)

            concat = tf.concat(toks_seen, axis=1)  # (?,width*attn_count)
            res_dense1 = tf.layers.dense(concat, self.width * m, activation=tf.nn.relu)  # (?,width*m)
            res_dense2 = tf.layers.dense(res_dense1, self.width, activation=tf.nn.relu)  # (?,width)

            res_dot = tf.expand_dims(emb_tok, axis=0) * tf.expand_dims(res_dense2, axis=1)  # (?,dict_size,width)
            logits = tf.reduce_mean(res_dot, axis=2, name='logits')  # (?,dict_size)

            predictions = {
                'logits': logits,
                'classes': tf.argmax(logits, axis=1, name='classes'),
            }
            loss = None
            if mode != tf.estimator.ModeKeys.PREDICT:
                loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits, weights=weights) + \
                       r * tf.square(tf.reduce_mean(tf.square(query)) - 1.0) + \
                       r * tf.reduce_mean(tf.square(tf.reduce_mean(tf.square(emb_tok), axis=1) - 1.0))

            if mode == tf.estimator.ModeKeys.TRAIN:
                optimizer = tf.train.AdamOptimizer(learning_rate=self.lr)
                train_op = optimizer.minimize(loss=loss, global_step=tf.train.get_global_step())

                return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op, predictions=predictions)
            elif mode == tf.estimator.ModeKeys.EVAL:
                eval_metric_ops = {
                    'accuracy': tf.metrics.accuracy(labels=labels, predictions=predictions['classes'])
                }
                return tf.estimator.EstimatorSpec(
                    mode=mode, loss=loss, eval_metric_ops=eval_metric_ops, predictions=predictions)
            else:
                return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

        return model_fn

    def __init__(self, lr=0.02, width=32, dict_size=40, attn_count=4, dense_multiplier=5, regularization=0.1):
        self.lr = lr
        self.width = width
        self.dict_size = dict_size
        self.attn_count = attn_count
        self.dense_multiplier = dense_multiplier
        self.regularization = regularization
        self.model_dir = '/tmp/querynet-w{}-d{}-a{}-m{}'.format(width, dict_size, attn_count, dense_multiplier)
        self.estimator = tf.estimator.Estimator(model_fn=self.build_model_fn(), model_dir=self.model_dir)
