PAD = '<PAD>'
DUMMY = '<DUMMY>'
QUERY = '<QUERY>'
POS_IN_FWD_PREFIX = '#pos_in_fwd_'
POS_IN_BWD_PREFIX = '#pos_in_bwd_'
POS_OUT_FWD_PREFIX = '#pos_out_fwd_'


def token_text(seq, out=False):
    return seq


def token_fwd_pos(seq, out=False):
    prefix = POS_OUT_FWD_PREFIX if out else POS_IN_FWD_PREFIX
    return [prefix + str(i) for i in range(len(seq))]


def token_bwd_pos(seq, out=False):
    res = [DUMMY for _ in seq]
    if out:
        return res

    pos = -1  # get last non PAD/QUERY
    for i in range(len(seq) - 1, -1, -1):
        if seq[i] not in (PAD, QUERY):
            pos = i
            break
    assert pos != -1

    # Start from last non PAD/QUERY. Iter backwards. Use negative addressing
    for i in range(len(seq)):
        prefix = POS_IN_BWD_PREFIX
        res[pos] = prefix + str(i)
        pos -= 1
    return res


property_funcs = [
    token_text,
    token_fwd_pos,
    token_bwd_pos,
]
