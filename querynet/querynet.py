from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import json

import numpy as np
import tensorflow as tf

WIDTH = 32
POS_SZ = 13
TOK_TYPES = 2
DICT_SZ = 100
LR = 0.02
TOKEN_PAD = '<PAD>'
META_PAD = '<META>'
QUERY = '<QUERY>'


def model_fn(features, labels, mode):
    x = features['x']
    query = tf.Variable(tf.random_uniform([WIDTH], -1, 1), name='emb_tok')
    emb_tok = tf.Variable(tf.random_uniform([DICT_SZ, WIDTH], -1, 1), name='emb_tok')

    t_emb = tf.nn.embedding_lookup(emb_tok, x[:, :, 0])
    m_emb = tf.nn.embedding_lookup(emb_tok, x[:, :, 1])
    tm_emb = t_emb + m_emb
    query_attn = tf.nn.softmax(tf.reduce_mean(tm_emb * query, axis=2), name='query_attn')
    query_seen = tf.reduce_mean(tf.expand_dims(query_attn, axis=2) * tm_emb, axis=1, name='query_seen')

    dense1 = tf.layers.dense(
        tf.layers.dense(query_seen, 5 * WIDTH, activation=tf.nn.relu), WIDTH, activation=tf.nn.relu)
    attn1 = tf.nn.softmax(tf.reduce_mean(tm_emb * tf.expand_dims(dense1, axis=1), axis=2), name='attn1')
    dense2 = tf.layers.dense(
        tf.layers.dense(query_seen, 5 * WIDTH, activation=tf.nn.relu), WIDTH, activation=tf.nn.relu)
    attn2 = tf.nn.softmax(tf.reduce_mean(tm_emb * tf.expand_dims(dense2, axis=1), axis=2), name='attn2')
    dense3 = tf.layers.dense(
        tf.layers.dense(query_seen, 5 * WIDTH, activation=tf.nn.relu), WIDTH, activation=tf.nn.relu)
    attn3 = tf.nn.softmax(tf.reduce_mean(tm_emb * tf.expand_dims(dense3, axis=1), axis=2), name='attn3')
    dense4 = tf.layers.dense(
        tf.layers.dense(query_seen, 5 * WIDTH, activation=tf.nn.relu), WIDTH, activation=tf.nn.relu)
    attn4 = tf.nn.softmax(tf.reduce_mean(tm_emb * tf.expand_dims(dense4, axis=1), axis=2), name='attn4')

    emb_attn1 = tf.reduce_mean(t_emb * tf.expand_dims(attn1, axis=2), axis=1, name='emb_attn1')
    emb_attn2 = tf.reduce_mean(t_emb * tf.expand_dims(attn2, axis=2), axis=1, name='emb_attn2')
    emb_attn3 = tf.reduce_mean(t_emb * tf.expand_dims(attn3, axis=2), axis=1, name='emb_attn3')
    emb_attn4 = tf.reduce_mean(t_emb * tf.expand_dims(attn4, axis=2), axis=1, name='emb_attn4')

    d = tf.layers.dense(
        tf.concat([emb_attn1, emb_attn2, emb_attn3, emb_attn4], axis=1),
        WIDTH * 5,
        activation=tf.nn.relu
    )
    d = tf.layers.dense(d, WIDTH, activation=tf.nn.relu)
    logits = tf.expand_dims(emb_tok, axis=0) * tf.expand_dims(d, axis=1)
    logits = tf.reduce_mean(logits, axis=2, name='logits')

    loss = None
    predictions = {
        'logits': logits,
        'classes': tf.argmax(logits, axis=1),
    }
    if mode == tf.estimator.ModeKeys.PREDICT:
        # predictions['labels_copy'] = features['labels_copy']
        predictions['x'] = features['x']
    else:
        loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)

    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.AdamOptimizer(learning_rate=LR)
        train_op = optimizer.minimize(loss=loss, global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op, predictions=predictions)
    elif mode == tf.estimator.ModeKeys.EVAL:
        eval_metric_ops = {
            'accuracy': tf.metrics.accuracy(labels=labels, predictions=predictions['classes'])
        }
        return tf.estimator.EstimatorSpec(
            mode=mode, loss=loss, eval_metric_ops=eval_metric_ops, predictions=predictions)
    else:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)


def read_dataset(file, limit=9999999):
    NTR = 0
    x, y = [], []
    with open(file, 'r') as f:
        for line in f:
            if limit <= 0:
                break
            else:
                limit -= 1
            o = json.loads(line)
            x.append(o['x'])
            y.append(o['y'])
            NTR += 1
    data = np.empty([NTR, POS_SZ, TOK_TYPES], dtype=np.int32)
    labels = np.empty([NTR], dtype=np.int32)
    for i in range(NTR):
        data[i] = x[i]
        labels[i] = y[i]
    return data, labels


def infer_util(querynet):
    with open('base/data/dict.txt', 'r') as dict_f:
        tokens_dict = json.loads(dict_f.readline().decode('utf8'))
    rev_tokens_dict = {v: k for k, v in tokens_dict.items()}

    def decode_dataset_item(item):
        return [[rev_tokens_dict[c] for c in t] for t in item]

    def pad(a, width=6):
        while len(a) < width:
            a.insert(0, TOKEN_PAD)
        return a

    def encode(s, pos, dir):
        return [tokens_dict[s], tokens_dict[dir + str(pos)]]

    def read_raw_dataset(file):
        lines_cnt = 0
        arr = []
        with open(file, 'r') as f:
            for line in f:
                lines_cnt += 1
                toks = pad([c for c in line.rstrip()])
                toks_enc = [encode(x, i, 'inp_') for i, x in enumerate(toks)]
                answ_enc = [encode(META_PAD if i + 1 < 7 else QUERY, i, 'outp_') for i in range(7)]
                arr.append(toks_enc + answ_enc)
        data = np.empty([lines_cnt, POS_SZ, TOK_TYPES], dtype=np.int32)
        for i in range(lines_cnt):
            data[i] = arr[i]
        return data

    data_test = read_raw_dataset('base/data/raw.txt')
    res = ['' for _ in range(len(data_test))]
    for i in range(7):
        test_input_fn = tf.estimator.inputs.numpy_input_fn(x={'x': data_test}, shuffle=False)
        predictions = querynet.predict(input_fn=test_input_fn)
        for j, prediction in enumerate(predictions):
            res[j] = rev_tokens_dict[prediction['classes']] + ' ' + res[j]
            data_test[j][-i - 2][0] = data_test[j][-i - 1][0]
            data_test[j][-i - 1][0] = prediction['classes']
    for i in range(len(res)):
        print(res[i].encode('utf8'))


def main(unused_argv):
    tf.logging.set_verbosity(tf.logging.INFO)
    querynet = tf.estimator.Estimator(model_fn=model_fn, model_dir="/tmp/querynet00")
    if unused_argv[-1] == 'infer':
        infer_util(querynet)
    else:
        data_train, labels_train = read_dataset('base/data/train.txt')
        data_eval, labels_eval = read_dataset('base/data/dev.txt')
        train_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={'x': data_train},
            y=labels_train,
            batch_size=4096,
            num_epochs=None,
            shuffle=True)
        eval_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={'x': data_eval},
            y=labels_eval,
            num_epochs=1,
            shuffle=False)
        for i in range(100):
            querynet.train(input_fn=train_input_fn, steps=1000)
            print(querynet.evaluate(input_fn=eval_input_fn))


if __name__ == "__main__":
    tf.app.run()
