from __future__ import division
from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals
import unittest
from lib import data


class DataTestCase(unittest.TestCase):
    def test_raw(self):
        data_raw = data.Raw.from_tsv('test_raw.txt')
        self.assertEqual(
            set(data_raw.dictionary().keys()),
            {'zero', 'one', 'twenty', '0', '1', '2'})

    def test_encoded(self):
        data_raw = data.Raw.from_tsv('test_raw.txt')
        data_encoded = data.Encoded.from_raw(data_raw)
        self.assertEqual(
            data_encoded.to_human_readable(),
            [
                [['0', '#pos_in_0'], ['zero', '#pos_out_0']],
                [['1', '#pos_in_0'], ['one', '#pos_out_0']],
                [['2', '#pos_in_0'], ['1', '#pos_in_1'], ['twenty', '#pos_out_0'], ['one', '#pos_out_1']]
            ]
        )

    def test_encoded_pad(self):
        data_raw = data.Raw.from_tsv('test_raw.txt')
        data_encoded = data.Encoded.from_raw(data_raw, pad_in=2, pad_out=3)
        self.assertEqual(
            data_encoded.to_human_readable(),
            [
                [['0', '#pos_in_0'], ['<PAD>', '#pos_in_1'],
                 ['zero', '#pos_out_0'], ['<PAD>', '#pos_out_1'], ['<PAD>', '#pos_out_2']],
                [['1', '#pos_in_0'], ['<PAD>', '#pos_in_1'],
                 ['one', '#pos_out_0'], ['<PAD>', '#pos_out_1'], ['<PAD>', '#pos_out_2']],
                [['2', '#pos_in_0'], ['1', '#pos_in_1'],
                 ['twenty', '#pos_out_0'], ['one', '#pos_out_1'], ['<PAD>', '#pos_out_2']]
            ]
        )

    def test_dataset(self):
        data_raw = data.Raw.from_tsv('test_raw.txt')
        data_encoded = data.Encoded.from_raw(data_raw)
        data_dataset = data.Dataset.from_encoded(data_encoded)
        self.assertEqual(
            data_dataset.to_human_readable(),
            [
                [
                    {'x': [['0', '#pos_in_0'], ['<QUERY>', '#pos_out_0']], 'y': 'zero'},
                ],
                [
                    {'x': [['1', '#pos_in_0'], ['<QUERY>', '#pos_out_0']], 'y': 'one'},
                ],
                [
                    {'x': [['2', '#pos_in_0'], ['1', '#pos_in_1'], ['<QUERY>', '#pos_out_0'],
                           ['<MASKED>', '#pos_out_1']],
                     'y': 'twenty'},
                    {'x': [['2', '#pos_in_0'], ['1', '#pos_in_1'], ['twenty', '#pos_out_0'], ['<QUERY>', '#pos_out_1']],
                     'y': 'one'},
                ],
            ]
        )


if __name__ == '__main__':
    unittest.main()
