from __future__ import print_function

import os

import tensorflow as tf
import numpy as np
from lib.querynet import QueryNet
from lib.data import Raw, Encoded, Dataset
from sklearn.metrics import accuracy_score
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

pad_in = 16
pad_out = 15


def convert_dataset(data_dataset, n, pad_in, pad_out):
    cnt = 0
    for rec in data_dataset.data:
        cnt += len(rec)
    x = np.empty([cnt, pad_in + pad_out, 3], dtype=np.int32)
    i = 0
    for rec in data_dataset.data:
        for pos in rec:
            x[i] = pos['x']
            i += 1
    y = np.empty([cnt], dtype=np.int32)
    i = 0
    for rec in data_dataset.data:
        for pos in rec:
            y[i] = pos['y']
            i += 1
    return x, y


def make_dict():
    raw_dict, len_in, len_out = Raw.dict_from_tsv_files([
        'base/data/cardinal_ru_train.txt', 'base/data/cardinal_ru_dev.txt'])
    combined = raw_dict
    tf.logging.debug(raw_dict)
    tf.logging.debug('')

    enc_dict = Encoded.make_dict(pad_in, pad_out)
    after_raw = max(combined.values()) + 1
    combined.update({k: after_raw + v for k, v in enc_dict.items()})
    tf.logging.debug(enc_dict)
    tf.logging.debug('')

    ds_dict = Dataset.make_dict()
    after_enc = max(combined.values()) + 1
    combined.update({k: after_enc + v for k, v in ds_dict.items()})
    tf.logging.debug(ds_dict)
    tf.logging.debug('')

    tf.logging.debug('len: ', len(combined))
    tf.logging.debug('')
    assert len(combined) == max(combined.values()) + 1
    tf.logging.debug(combined)
    return combined, len_in, len_out


def main(unused_argv):
    tf.logging.set_verbosity(tf.logging.DEBUG)

    md, len_in, len_out = make_dict()
    tf.logging.debug(len_in)
    tf.logging.debug(len_out)
    d = {
        u'\u043c\u0438\u043b\u043b\u0438\u043e\u043d\u043e\u0432': 20, u'\u0441\u0435\u043c\u043d\u0430\u0434\u0446\u0430\u0442\u0438': 89,
        u'\u043f\u044f\u0442\u044c\u044e\u0441\u0442\u0430\u043c\u0438': 85, u'-': 25, u'V': 140, u'\u0448\u0435\u0441\u0442\u0438': 65,
        u'\u043e\u0434\u0438\u043d': 11, u'\u0442\u044b\u0441\u044f\u0447\u0435\u0439': 32, u'#pos_in_fwd_9': 210,
        u'\u0442\u044b\u0441\u044f\u0447\u0438': 27, u'X': 139, u'#pos_in_fwd_5': 202, u'#pos_in_fwd_4': 200,
        u'\u0447\u0435\u0442\u044b\u0440\u0435\u0441\u0442\u0430': 49, u'#pos_in_fwd_6': 204, u'#pos_in_fwd_1': 194, u'#pos_in_fwd_0': 192,
        u'#pos_in_fwd_3': 198, u'27-\u0442\u0438': 185, u'#pos_in_bwd_9': 211, u'\u0441\u0442\u0430': 94,
        u'\u043c\u0438\u043b\u043b\u0438\u0430\u0440\u0434': 78, u'\u0434\u0432\u0443\u043c': 169,
        u'\u0432\u043e\u0441\u0435\u043c\u044c\u0441\u043e\u0442': 67, u'#pos_out_fwd_12': 236, u'#pos_in_bwd_1': 195,
        u'\u043f\u044f\u0442\u0438\u0441\u0442\u0430\u043c': 172, u'#pos_in_bwd_3': 199, u'#pos_in_bwd_2': 197, u'#pos_in_bwd_5': 203,
        u'#pos_in_bwd_4': 201, u'#pos_in_bwd_13': 219, u'#pos_in_bwd_6': 205, u'blen12': 86, u'blen13': 0, u'blen10': 56, u'blen11': 150,
        u'\u043f\u044f\u0442\u044c': 57, u'4': 5, u'blen14': 134, u'blen15': 77, u'8': 3,
        u'\u0432\u043e\u0441\u0435\u043c\u043d\u0430\u0434\u0446\u0430\u0442\u044c\u044e': 130,
        u'\u0448\u0435\u0441\u0442\u044c\u044e': 155, u'\u043c': 148, u'\u043f\u044f\u0442\u0438': 76, u'\u044b_trans': 176,
        u'#pos_in_fwd_15': 222, u'#pos_in_fwd_14': 220, u'#pos_in_fwd_11': 214, u'#pos_in_fwd_10': 212,
        u'\u043e\u0434\u043d\u0438\u043c': 164, u'#pos_in_fwd_12': 216, u'\u043f\u044f\u0442\u0438\u0441\u0442\u0430\u0445': 124, u'9': 1,
        u'\u0434\u0432\u0443\u0445\u0441\u0442\u0430\u0445': 152, u'\u0434\u0435\u0432\u044f\u043d\u043e\u0441\u0442\u043e': 43,
        u'51-\u0442\u0438': 179, u'\u0434\u0435\u0432\u044f\u043d\u043e\u0441\u0442\u0430': 73,
        u'\u043e\u0434\u0438\u043d\u043d\u0430\u0434\u0446\u0430\u0442\u044c\u044e': 144, u'#pos_out_fwd_3': 227, u'#pos_out_fwd_2': 226,
        u'#pos_out_fwd_1': 225, u'#pos_out_fwd_0': 224, u'#pos_out_fwd_7': 231, u'#pos_out_fwd_6': 230, u'#pos_out_fwd_5': 229,
        u'#pos_out_fwd_4': 228, u'#pos_out_fwd_9': 233, u'#pos_out_fwd_8': 232,
        u'\u0441\u0435\u043c\u043d\u0430\u0434\u0446\u0430\u0442\u044c': 39,
        u'\u0448\u0435\u0441\u0442\u044c\u044e\u0441\u0442\u0430\u043c\u0438': 33,
        u'\u0441\u0435\u043c\u043d\u0430\u0434\u0446\u0430\u0442\u044c\u044e': 158, u'-0': 183,
        u'\u043c\u0438\u043b\u043b\u0438\u0430\u0440\u0434\u043e\u0432': 58, u'\u0448\u0435\u0441\u0442\u044c': 13, u'#pos_out_fwd_11': 235,
        u'#pos_out_fwd_10': 234, u'\u0442_trans': 175, u'#pos_in_fwd_8': 208, u'\u043e\u0434\u043d\u0438': 186,
        u'\u0434\u0432\u0435\u043d\u0430\u0434\u0446\u0430\u0442\u044c\u044e': 154, u'\u0442\u044b\u0441\u044f\u0447\u0430\u043c': 160,
        u'\u0441\u0435\u043c\u044c\u044e': 143, u'\u0448\u0435\u0441\u0442\u043d\u0430\u0434\u0446\u0430\u0442\u044c': 54,
        u'\u0442\u0440\u0435\u043c': 171, u'\u0434\u0435\u0432\u044f\u0442\u044c\u0441\u043e\u0442': 66,
        u'\u0434\u0435\u0432\u044f\u0442\u0438\u0441\u0442\u0430\u0445': 151,
        u'\u0434\u0435\u0432\u044f\u0442\u043d\u0430\u0434\u0446\u0430\u0442\u044c': 118,
        u'\u0432\u043e\u0441\u0435\u043c\u044c\u0434\u0435\u0441\u044f\u0442': 60, u'#pos_in_fwd_7': 206,
        u'\u0432\u043e\u0441\u0435\u043c\u043d\u0430\u0434\u0446\u0430\u0442\u044c': 68,
        u'\u0447\u0435\u0442\u044b\u0440\u043d\u0430\u0434\u0446\u0430\u0442\u044c': 84, u'\u043f\u044f\u0442\u044c\u0441\u043e\u0442': 50,
        u'\u043f\u044f\u0442\u043d\u0430\u0434\u0446\u0430\u0442\u0438': 96, u'\u0442\u0440\u0435\u0445\u0441\u0442\u0430\u0445': 112,
        u'#pos_in_fwd_13': 218, u'\u0441\u0435\u043c\u0438\u0441\u0442\u0430\u0445': 159,
        u'\u043c\u0438\u043b\u043b\u0438\u043e\u043d\u0430': 59, u'\u0434\u0435\u0432\u044f\u0442\u044c': 8, u'I': 141, u'3': 16,
        u'#pos_in_bwd_8': 209, u'7': 2, u'\u0441\u043e\u0440\u043e\u043a\u0430': 75, u'\u0434\u0432\u0430': 40, u'#pos_in_fwd_2': 196,
        u'\u0434\u0432\u0435': 69, u'<MASKED>': 241, u'\u0434\u0435\u0432\u044f\u0442\u0438': 52,
        u'\u043c\u0438\u043b\u043b\u0438\u0430\u0440\u0434\u0430': 87, u'\u0447\u0435\u0442\u044b\u0440\u044c\u043c\u044f': 157,
        u'\u044f': 149, u'<DUMMY>': 239, u'\u0434\u0435\u0432\u044f\u0442\u043d\u0430\u0434\u0446\u0430\u0442\u044c\u044e': 182,
        u'\u0447\u0435\u0442\u044b\u0440\u043d\u0430\u0434\u0446\u0430\u0442\u0438': 136,
        u'\u0442\u0440\u0438\u0434\u0446\u0430\u0442\u0438': 135, u'\u043f\u044f\u0442\u043d\u0430\u0434\u0446\u0430\u0442\u044c': 70,
        u'\u0434\u0432\u0443\u0445': 71, u'\u043f\u044f\u0442\u044c\u044e': 80, u'\u0434\u0432\u0435\u0441\u0442\u0438': 47,
        u'\u0442\u044b\u0441\u044f\u0447\u044c\u044e': 123, u'\u0434\u0432\u0430\u0434\u0446\u0430\u0442\u044c\u044e': 83,
        u'\u0442\u0440\u0435\u0445\u0441\u043e\u0442': 53, u'\u0434\u0435\u0432\u044f\u0442\u043d\u0430\u0434\u0446\u0430\u0442\u0438': 125,
        u'\u0432\u043e\u0441\u044c\u043c\u0438': 99, u'\u0447\u0435\u0442\u044b\u0440\u0435': 12,
        u'\u0441\u0435\u043c\u0438\u0434\u0435\u0441\u044f\u0442\u0438': 51,
        u'\u0434\u0435\u0432\u044f\u0442\u0438\u0441\u0442\u0430\u043c': 119,
        u'\u0432\u043e\u0441\u0435\u043c\u043d\u0430\u0434\u0446\u0430\u0442\u0438': 114, u'\u0442\u0440\u0435\u0445': 105,
        u'#pos_in_bwd_0': 193, u'\u043e\u0434\u043d\u043e\u043c\u0443': 120, u'#pos_out_fwd_14': 238,
        u'\u043f\u044f\u0442\u044c\u044e\u0434\u0435\u0441\u044f\u0442\u044c\u044e': 102,
        u'\u0448\u0435\u0441\u0442\u043d\u0430\u0434\u0446\u0430\u0442\u0438': 145, u'\u0442\u044b\u0441\u044f\u0447\u0443': 92,
        u'\u0434\u0435\u0441\u044f\u0442\u044c\u044e': 180, u'\u0442\u0440\u0438\u043d\u0430\u0434\u0446\u0430\u0442\u044c\u044e': 168,
        u'\u0434\u0432\u0435\u043d\u0430\u0434\u0446\u0430\u0442\u0438': 142,
        u'\u043e\u0434\u0438\u043d\u043d\u0430\u0434\u0446\u0430\u0442\u0438': 104, u'\u043e\u0434\u043d\u043e\u043c': 153,
        u'\u0441\u0435\u043c\u044c': 9, u'#pos_in_bwd_7': 207, u'\u0432\u043e\u0441\u044c\u043c\u0438\u0441\u043e\u0442': 63,
        u'\u043c\u0438\u043b\u043b\u0438\u043e\u043d': 91, u'#pos_out_fwd_13': 237, u'\u0434\u0432\u0430\u0434\u0446\u0430\u0442\u0438': 64,
        u'\u0432\u043e\u0441\u044c\u043c\u044c\u044e\u0434\u0435\u0441\u044f\u0442\u044c\u044e': 131,
        u'\u0441\u0435\u043c\u044c\u044e\u0434\u0435\u0441\u044f\u0442\u044c\u044e': 34,
        u'\u0442\u0440\u0438\u043d\u0430\u0434\u0446\u0430\u0442\u044c': 48, u'0': 7, u'\u043d\u043e\u043b\u044c': 14,
        u'\u0434\u0435\u0441\u044f\u0442\u044c': 79, u'\u0448\u0435\u0441\u0442\u044c\u044e\u0434\u0435\u0441\u044f\u0442\u044c\u044e': 163,
        u'2': 30, u'\u0448\u0435\u0441\u0442\u0438\u0441\u0442\u0430\u043c': 189, u'\u0442\u044b\u0441\u044f\u0447': 37, u'6': 6,
        u'\u0440_trans': 177, u'<QUERY>': 240, u'\u0434\u0432\u0443\u043c\u0441\u0442\u0430\u043c': 188,
        u'\u0441\u0435\u043c\u044c\u0434\u0435\u0441\u044f\u0442': 55, u'\u0442': 115,
        u'\u0447\u0435\u0442\u044b\u0440\u043d\u0430\u0434\u0446\u0430\u0442\u044c\u044e': 190,
        u'\u0448\u0435\u0441\u0442\u0438\u0434\u0435\u0441\u044f\u0442\u0438': 95,
        u'\u0434\u0435\u0432\u044f\u0442\u0438\u0441\u043e\u0442': 113, u'\u0442\u0440\u0438\u0441\u0442\u0430': 18,
        u'\u0447\u0435\u0442\u044b\u0440\u0435\u0445': 93, u'\u0442\u0440\u0438\u043d\u0430\u0434\u0446\u0430\u0442\u0438': 133,
        u'\u0447\u0435\u0442\u044b\u0440\u0435\u0445\u0441\u043e\u0442': 100, u'\u0438': 111, u'\u0434\u0435\u0441\u044f\u0442\u0438': 98,
        u'\u0441\u0435\u043c\u044c\u0441\u043e\u0442': 61, u'\u0441\u0435\u043c\u0438': 74, u'#pos_in_bwd_10': 213, u'4-\u0438': 165,
        u'\u0434\u0432\u0430\u0434\u0446\u0430\u0442\u044c': 31, u'\u0442\u0440\u0435\u043c\u044f\u0441\u0442\u0430\u043c\u0438': 166,
        u'\u0432\u043e\u0441\u044c\u043c\u0438\u0434\u0435\u0441\u044f\u0442\u0438': 116, u'blen1': 109,
        u'\u0447\u0435\u0442\u044b\u0440\u0435\u0445\u0441\u0442\u0430\u0445': 101,
        u'\u043c\u0438\u043b\u043b\u0438\u043e\u043d\u0430\u043c\u0438': 146, u'\u043f\u044f\u0442\u044c\u0434\u0435\u0441\u044f\u0442': 19,
        u'\u0435_trans': 174, u'\u0434\u0432\u0435\u043d\u0430\u0434\u0446\u0430\u0442\u044c': 36, u'#pos_in_bwd_14': 221,
        u'\u0447_trans': 173, u'\u043f\u044f\u0442\u0438\u0441\u043e\u0442': 88, u'#pos_in_bwd_11': 215,
        u'\u0442\u0440\u0435\u043c\u044f': 138, u'\u043f\u044f\u0442\u043d\u0430\u0434\u0446\u0430\u0442\u044c\u044e': 170,
        u'#pos_in_bwd_12': 217, u'#pos_in_bwd_15': 223, u'\u0432\u043e\u0441\u044c\u043c\u0438\u0441\u0442\u0430\u0445': 128,
        u'\u043c\u0438\u043d\u0443\u0441': 26, u'\u043f\u044f\u0442\u0438\u0434\u0435\u0441\u044f\u0442\u0438': 110,
        u'\u0434\u0435\u0432\u044f\u0442\u044c\u044e\u0441\u0442\u0430\u043c\u0438': 162, u'blen8': 46, u'blen9': 15,
        u'\u0447\u0435\u0442\u044b\u0440\u0435\u043c\u0441\u0442\u0430\u043c': 161, u'\u0442\u044b\u0441\u044f\u0447\u0435': 181,
        u'\u0442\u044b\u0441\u044f\u0447\u0430\u043c\u0438': 81, u'\u043c\u0438\u043b\u043b\u0438\u0430\u0440\u0434\u0430\u0445': 184,
        u'\u0442\u044b\u0441\u044f\u0447\u0430': 23, u'\u0448\u0435\u0441\u0442\u044c\u0441\u043e\u0442': 29, u'blen2': 117, u'blen3': 44,
        u'blen4': 28, u'blen5': 24, u'blen6': 62, u'blen7': 90, u'1': 4, u'\u043e\u0434\u043d\u0430': 22, u'<PAD>': 191,
        u'\u0434\u0432\u0443\u043c\u044f\u0441\u0442\u0430\u043c\u0438': 147, u'5': 17, u'\u0432\u043e\u0441\u0435\u043c\u044c': 10,
        u'\u0441\u0435\u043c\u044c\u044e\u0441\u0442\u0430\u043c\u0438': 167, u'\u0442\u0440\u0438\u0434\u0446\u0430\u0442\u044c': 45,
        u'\u0441\u0442\u043e': 38, u'\u043e\u0434\u043d\u043e': 129, u'\u043e\u0434\u043d\u0443': 108,
        u'\u0434\u0432\u0443\u0445\u0441\u043e\u0442': 97, u'\u0448\u0435\u0441\u0442\u044c\u0434\u0435\u0441\u044f\u0442': 21,
        u'\u0432\u043e\u0441\u044c\u043c\u044c\u044e': 103, u'\u043e\u0434\u043d\u043e\u0439': 106,
        u'\u0432\u043e\u0441\u044c\u043c\u044c\u044e\u0441\u0442\u0430\u043c\u0438': 82,
        u'\u0448\u0435\u0441\u0442\u0438\u0441\u0442\u0430\u0445': 126, u'\u0442\u0440\u0438\u0434\u0446\u0430\u0442\u044c\u044e': 137,
        u'\u043e\u0434\u0438\u043d\u043d\u0430\u0434\u0446\u0430\u0442\u044c': 107, u'\u0448\u0435\u0441\u0442\u0438\u0441\u043e\u0442': 72,
        u'\u0442\u044b\u0441\u044f\u0447\u0430\u0445': 127,
        u'\u0447\u0435\u0442\u044b\u0440\u044c\u043c\u044f\u0441\u0442\u0430\u043c\u0438': 156,
        u'\u0441\u0435\u043c\u0438\u0441\u043e\u0442': 122, u'\u0434\u0435\u0432\u044f\u0442\u044c\u044e': 132,
        u'\u0432\u043e\u0441\u044c\u043c\u0438\u0441\u0442\u0430\u043c': 187, u'\u0441\u043e\u0440\u043e\u043a': 42,
        u'\u0442\u0440\u0438': 41, u'\u043c\u0438\u043b\u043b\u0438\u043e\u043d\u0430\u0445': 178,
        u'\u043e\u0434\u043d\u043e\u0433\u043e': 121, u'\u0434\u0432\u0443\u043c\u044f': 35}
    assert md == d
    assert pad_in == len_in
    assert pad_out == len_out
    dict_size = len(d)
    dense_multiplier = int(os.environ.get('DENSE_MULTIPLIER', 5))
    attn_count = int(os.environ.get('ATTN_COUNT', 5))
    querynet = QueryNet(lr=0.02, dict_size=dict_size, width=256, attn_count=attn_count, dense_multiplier=dense_multiplier)
    tf.logging.debug("Model created")
    rd = {v: k for k, v in d.items()}
    assert len(d) == len(rd)

    if len(unused_argv) > 1 and unused_argv[1] == 'infer':
        tf.logging.set_verbosity(tf.logging.FATAL)
        raw = Raw.from_tsv(unused_argv[2] if len(unused_argv) > 2 else 'base/data/dev.txt')
        encoded = Encoded.from_raw(raw, dict=d, pad_in=pad_in, pad_out=pad_out)
        y_true = [[rd[x[0]] for x in el[pad_in:]] for el in encoded.data]
        dev_dataset = Dataset.from_encoded(encoded, 1)
        x_dev, y_dev = convert_dataset(dev_dataset, len(dev_dataset.data), pad_in, pad_out)
        for i in range(pad_out):
            eval_input_fn = tf.estimator.inputs.numpy_input_fn(
                x={'x': x_dev},
                num_epochs=1,
                shuffle=False)
            l = list(querynet.estimator.predict(input_fn=eval_input_fn))

            for j in range(len(x_dev)):
                if i + 1 < pad_out:
                    x_dev[j][pad_in + i + 1][0] = x_dev[j][pad_in + i][0]
                x_dev[j][pad_in + i][0] = l[j]['classes']

        y_pred = [[rd[x[0]] for x in el[pad_in:]] for el in x_dev]
        print(y_true[0])
        print(y_pred[0])
        print(accuracy_score([' '.join(x) for x in y_true], [' '.join(x) for x in y_pred]))
        err_cnt = 0
        for t, p in zip([' '.join(x) for x in y_true], [' '.join(x) for x in y_pred]):
            if t != p and err_cnt < 20:
                print('t', t.encode('utf8'))
                print('p', p.encode('utf8'))
                print()
            if t != p:
                err_cnt += 1
        print(err_cnt)
        pass
    elif len(unused_argv) > 1 and unused_argv[1] == 'eval':
        raw = Raw.from_tsv(unused_argv[2] if len(unused_argv) > 2 else 'base/data/dev.txt')
        dev_dataset = Dataset.from_encoded(
            Encoded.from_raw(raw, dict=d, pad_in=pad_in, pad_out=pad_out))
        x_dev, y_dev = convert_dataset(dev_dataset, len(dev_dataset.data), pad_in, pad_out)
        eval_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={'x': x_dev},
            y=y_dev,
            num_epochs=1,
            shuffle=False)
        print(querynet.estimator.evaluate(input_fn=eval_input_fn))
    else:
        tf.logging.debug(d)
        raw = Raw.from_tsv('base/data/cardinal_ru_train.txt')
        # print(raw.data[0])
        enc = Encoded.from_raw(raw, dict=d, pad_in=pad_in, pad_out=pad_out)
        # for el in enc.to_human_readable()[:10]:
        #     print(el)
        # assert False
        train_dataset = Dataset.from_encoded(enc)
        # for el in train_dataset.data[0]:
        #     print(el)
        x_train, y_train = convert_dataset(train_dataset, len(train_dataset.data), pad_in, pad_out)
        # print(y_train[1])
        # print(x_train[1])
        # print(train_dataset.to_human_readable()[0][1])
        # assert False

        dev_dataset = Dataset.from_encoded(
            Encoded.from_raw(Raw.from_tsv('base/data/cardinal_ru_dev.txt'), dict=d, pad_in=pad_in, pad_out=pad_out))
        x_dev, y_dev = convert_dataset(dev_dataset, len(dev_dataset.data), pad_in, pad_out)
        # print(y_dev[:20])
        tf.logging.debug(len(dev_dataset.dict))
        assert len(dev_dataset.dict) == dict_size
        assert len(train_dataset.dict) == dict_size

        tensors_to_log = [
            'classes',
            'logits',
            'labels',
        ]
        logging_hook = tf.train.LoggingTensorHook(
            tensors=tensors_to_log, every_n_iter=500)
        train_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={'x': x_train},
            y=y_train,
            batch_size=4096,
            num_epochs=None,
            shuffle=True)
        eval_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={'x': x_dev},
            y=y_dev,
            num_epochs=1,
            shuffle=False)
        for i in range(5):
            querynet.estimator.train(
                input_fn=train_input_fn, steps=1000,
                # hooks=[logging_hook],
            )
            print(querynet.estimator.evaluate(input_fn=eval_input_fn))


if __name__ == "__main__":
    tf.app.run()
